<?php
/**
 * @file theme-settings.php
 * Provides the settings for the Camaxtli theme.
 *
 */

/**
 * Implementation of THEMEHOOK_settings().
 *
 * @param array $settings An array of saved settings for this
 *        theme.
 *
 * @return array A form array.
 */
function phptemplate_settings($settings) {
  $form = array();

  // --------------------------------------------------------------------------
  // -- Strip the ' (not verified)' from output usernames?
  $default = $settings['camaxtli_remove_not_verified'];
  $default = empty($default) ? FALSE : (boolean)$default;
  $desc    = t("Normally, when an anonymous visitors posts a comment, their name is suffixed with '%verified'. Checking this will prevent that text from being added.",
               array('%verified' => ' (not verified)'));
  $form['camaxtli_remove_not_verified'] = array(
    '#type'          => 'checkbox',
    '#title'         => t("Strip '(not verified)' from usernames"),
    '#default_value' => $default,
    '#description'   => $desc,
  );

  // --------------------------------------------------------------------------
  // -- How many page items to put in the pager widgets.
  $default = $settings['camaxtli_pager_page_count'];
  $default = !empty($default) ? intval($default) : 5;
  $options = range(3, 10);
  $desc    = t('The number of default pages to include in pager controls. If you are using a three column layout, a lower number here will work better.');

  $form['camaxtli_pager_page_count'] = array(
    '#type'           => 'select',
    '#title'          => t('Pager item count'),
    '#options'        => drupal_map_assoc($options),
    '#default_value'  => $default,
    '#description'    => $desc,
  );

  return $form;
}
