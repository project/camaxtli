<?php
/**
 * @file comment.tpl.php
 * Comment rendering for Camaxtli.
 *
 */
?>
<div class="<?php print $camaxtli_comment_class; ?>">
  <div class="content clear-block">
    <div class="comment-meta grid-4 alpha">
      <div class="comment-on comment-author meta-div"><?php print $author; ?></div>
      <?php if ($comment->new): ?>
        <div class="new meta-div"><?php print drupal_ucfirst($new); ?></div>
      <?php endif; ?>
      <div class="timestamp meta-div"><?php print $camaxtli_comment_date; ?></div>
      <?php if ($camaxtli_comment_links): ?>
        <div class="comment-links meta-div"><?php print $camaxtli_comment_links; ?></div>
      <?php endif; ?>
    </div><!-- /comment-meta -->
    <div class="comment-body grid-8 omega">
      <?php print $content ?>
    </div><!-- /comment-body -->
  </div>
</div>