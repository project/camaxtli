<?php
/**
 * @file page.tpl.php
 * The variable-layout page structure for Camaxtli.
 *
 * In addition to the standard variables Drupal makes available to page.tpl.php,
 * these variables are made available by the theme:
 *
 */
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <?php print $styles; ?>
    <?php print $scripts; ?>
    <!--[if lt IE 8]>
        <?php print phptemplate_get_ie_styles() . "\n"; ?>
    <![endif]-->
  </head>
  <body class="camaxtli <?php print $body_classes; ?>">
    <div id="header-bar" class="clear-block">
      <div id="header" class="container-12">
        <div id="title" class="grid-12 alpha omega">
          <?php if (isset($camaxtli_title)) { print $camaxtli_title; } ?>
        </div><!-- /title -->
      </div><!-- /header -->
    </div><!-- /header-bar -->
    <div id="page-bar" class="clear-block">
       <div id="page-title-wrap" class="container-12">
         <?php if (!empty($camaxtli_page_title)) { print $camaxtli_page_title; } ?>
       </div><!-- /page-title-wrap -->
    </div><!-- /page-bar -->
    <div id="content-bar" class="clear-block">
      <div id="content-container" class="container-12">
        <div id="content" class="<?php print $camaxtli_classes['content']; ?>">
          <?php if ($show_messages && !empty($messages)) { print $messages; } ?>
          <?php print $help; ?>
          <?php if (!empty($tabs)):?>
            <div id="tabs-wrapper" class="<?php print $camaxtli_content_width; ?> alpha omega clear-block">
              <ul class="tabs primary clear-block<?php if ($tabs2) { print ' has-secondary'; } ?>"><?php print $tabs; ?></ul>
                <?php if ($tabs2): ?>
                  <ul class="tabs secondary"><?php print $tabs2; ?></ul>
                <?php endif; ?>
            </div>
            <hr class="break"/>
          <?php endif; ?>
          <?php print $content; ?>
        </div><!-- /content -->
      </div><!-- /content-container -->
    </div><!-- /content-bar -->
    <div id="bottom-bar" class="clear-block">
      <div id="bottom-wrap" class="container-12">
        <div id="bottom-left" class="grid-4 alpha">
          <?php if (isset($bottom_left)) { print $bottom_left; } ?>
        </div><!-- /bottom-left -->
        <div id="bottom-center" class="grid-4">
          <?php if (isset($bottom_center)) { print $bottom_center; } ?>
        </div><!-- /bottom-center -->
        <div id="bottom-right" class="grid-4 omega">
          <?php if (isset($bottom_right)) { print $bottom_right; } ?>
        </div><!-- /bottom-right -->
        <hr class="break"/>
        <div id="footer" class="grid-12 alpha omega clear-block">
          <?php print $footer_message . $footer; ?>
        </div><!-- /footer -->
      </div><!-- /bottom-wrap -->
    </div><!-- /bottom-bar -->
    <?php print $closure; ?>
  </body>
</html>