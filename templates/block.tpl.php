<?php
/**
 * @file block.tpl.php
 * Block rendering for Camaxtli.
 *
 * In addition to the standard variables Drupal makes available to node.tpl.php,
 * these variables are made available by the theme:
 *
 * - $camaxtli_block_id - A complete unique ID for the block.
 * - $camaxtli_block_class - The CSS classes to apply to the block. If the Block
 *          Class module is installed, those classes will be added to these.
 *
 */
?>
<div id="<?php print $camaxtli_block_id; ?>" class="<?php print $camaxtli_block_class; ?>">
  <?php if (!empty($block->subject)): ?>
  	<h2><?php print $block->subject ?></h2>
  <?php endif;?>
  <?php print $block->content ?>
</div><!-- /<?php print $camaxtli_block_id; ?> -->
<br class="break"/>