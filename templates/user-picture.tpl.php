﻿<?php
/**
 * @file user-picture.tpl.php
 * Wraps the user picture in a div with a CSS class.
 *
 */
?>
<div class="user-picture">
<?php print $picture; ?>
</div>