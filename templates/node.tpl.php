<?php
/**
 * @file node.tpl.php
 * The node rendering logic for Camaxtli.
 *
 * In addition to the standard variables Drupal makes available to node.tpl.php,
 * these variables are made available by the theme:
 *
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php echo $camaxtli_node_class; ?>">
  <div id="node-<?php print $node->nid; ?>-meta" class="meta grid-4 alpha">
    <?php if ($page == 0): ?>
      <div class="node-headline clear-block">
        <h2><a href="<?php print $node_url; ?>" rel="bookmark" title="<?php print $camaxtli_perma_title; ?>"><?php print $title; ?></a></h2>
      </div><!-- /node-headline -->
    <?php endif; ?>
    <!-- <div class="node-author"><?php print $camaxtli_node_author; ?></div> -->
    <div class="timestamp meta-div"><?php print $camaxtli_node_timestamp; ?></div>
    <?php print $picture; ?>
    <?php if (!empty($taxonomy)): ?>
      <div class="node-terms meta-div"><?php print $camaxtli_term_links; ?></div><!-- /node-terms -->
    <?php endif; ?>
    <?php if (!empty($camaxtli_comment)): ?>
      <div class="comment-on meta-div"><?php print $camaxtli_comment; ?></div><!-- /node-comments -->
    <?php endif; ?>
    <?php if (!empty($taxonomy) || !empty($links)): ?>
      <?php
      if (!empty($links)): ?>
          <div class="links"><?php print $links; ?></div>
      <?php endif; ?>
    <?php endif; ?>
  </div><!-- /node.meta -->
  <div class="content grid-8 omega">
    <?php print $content; ?>
    <?php if (!empty($camaxtli_readmore)): ?>
        <p class="readmore"><?php print $camaxtli_readmore; ?></p>
    <?php endif; ?>
  </div><!-- /content -->
</div> <!-- /node -->