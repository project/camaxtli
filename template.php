<?php
/**
 * @file template.php
 * The core functions for the Nitobe theme.
 *
 */

/**
 * Return the path to the main Nitobe theme directory.
 *
 * @return The path to Nitobe.
 */
function camaxtli_theme_path() {
  static $theme_path;

  if (!isset($theme_path)) {
    global $theme;
    if ($theme == 'camaxtli') {
      $theme_path = path_to_theme();
    } else {
      $theme_path = drupal_get_path('theme', 'camaxtli');
    }
  }

  return $theme_path;
}


/**
 * Implementation of hook_theme(). Registers Camaxtli's overrides.
 *
 * @param $existing An array of existing implementations that may be used for
 *        override purposes.
 * @param $type What 'type' is being processed. May be one of: module,
 *        base_theme_engine, theme_engine, base_theme, theme
 * @param $theme The actual name of theme that is being being checked.
 * @param $path The directory path of the theme or module, so that it doesn't
 *        need to be looked up.
 *
 * @return array
 */
function camaxtli_theme($existing, $type, $theme, $path) {
  $funcs = array(
    'camaxtli_username' => array(
      'arguments' => $existing['theme_username'],
    ),
    'camaxtli_preprocess_block' => array(
      'arguments' => $existing['phptemplate_preprocess_block'],
    ),
    'camaxtli_preprocess_page' => array(
      'arguments' => $existing['phptemplate_preprocess_page'],
    ),
    'camaxtli_preprocess_maintenance_page' => array(
      'arguments' => $existing['phptemplate_preprocess_maintenance_page'],
    ),
    'camaxtli_pager' => array(
      'arguments' => $existing['theme_pager'],
    ),
    'camaxtli_preprocess_comment' => array(
      'arguments' => $existing['phptemplate_preprocess_comment'],
    ),
    'camaxtli_preprocess_node' => array(
      'arguments' => $existing['phptemplate_preprocess_page'],
    ),
    'comment_form' => array(
      'arguments' => array('form' => NULL),
    ),
    'camaxtli_box' => array(
      'arguments' => $existing['theme_box'],
    ),
  );

  return $funcs;
}


/**
 * Override theme_box so that the comment form isn't boxed.
 *
 * @param $title The subject of the box.
 * @param $content The content of the box.
 * @param $region The region in which the box is displayed.
 *
 * @return A string containing the box output.
 */
function camaxtli_box($title, $content, $region = 'main') {
  if ($title != t('Post new comment')) {
    return theme_box($title, $content, $region);
  } else {
    return $content;
  }
}


/**
 * Override the rendering of the comment form.
 *
 * @param $form The form elements to render.
 *
 * @return String containing the rendered HTML for the comment form.
 */
function camaxtli_comment_form($form) {
  $prefix = '<div id="comment-form-wrap">' .
            '<h2 id="comment-form-title" class="grid-4 alpha">' .
            t('Post a comment') . '</h2>' .
            '<div class="grid-8 omega">';
  $suffix = '</div></div>';

  return $prefix . drupal_render($form) . $suffix;
}


/**
 * Override of theme_pager(). Alters the default quantity of pager items.
 * @param $tags An array of labels for the controls in the pager.
 * @param $limit The number of query results to display per page.
 * @param $element An optional integer to distinguish between multiple pagers
 *        on one page.
 * @param $parameters An associative array of query string parameters to append
 *        to the pager links.
 * @param $quantity The number of page items to show in the pager. If this
 *        value is zero (0), the item count specified by the theme setting
 *        camaxtli_pager_page_count will be used (5 if not set).
 *
 * @return An HTML string that generates the query pager.
 */
function camaxtli_pager($tags = array(), $limit = 10, $element = 0, $parameters = array(), $quantity = 0) {
  if ($quantity == 0) {
    $quantity = theme_get_setting('camaxtli_pager_page_count', 5);
    $quantity = empty($quantity) ? 5 : $quantity;
  }

  return theme_pager($tags, $limit, $element, $parameters, $quantity);
}


/**
 * Decorates theme_username() to strip the " (not verified)" string from the
 * commenter's name.
 *
 * @param $account An instance of a user object.
 *
 * @return The altered HTML output from theme_username().
 */
function camaxtli_username($account) {
  $output = theme_username($account);

  if ((boolean)theme_get_setting('camaxtli_remove_not_verified')) {
    $to_strip = ' ('. t('not verified') .')';
    $output   = str_replace($to_strip, '', $output);
  }

  return $output;
}

/**
 * Determine whether to show the date stamp for the given node.
 *
 * @param $type The machine readable name of the type to check.
 *
 * @return TRUE if the node is of a type that should show the date stamp, FALSE
 *         if not.
 */
function camaxtli_show_datestamp($type) {
  $default     = drupal_map_assoc(array('blog', 'forum', 'poll', 'story'));
  $valid_types = theme_get_setting('camaxtli_show_datestamp');
  $valid_types = (!empty($valid_types)) ? $valid_types : $default;

  return (array_key_exists($type, $valid_types) && ($valid_types[$type] === $type));
}


/**
 * Removes the spaces between words in the given string and returns an HTML
 * string with every other word wrapped in a span with the class "alt-color".
 *
 * @param $title The text to render.
 *
 * @return The rendered HTML.
 */
function camaxtli_title_effect($title = '') {
  $words  = explode(' ', $title);
  $result = '';

  if (is_array($words)) {
    $alt = FALSE;
    foreach ($words as $word) {
      if ($alt) {
        $result .= '<span class="alt-color">' . $word . '</span>';
      } else {
        $result .= $word;
      }

      $alt = !$alt;
    }
  }

  return $result;
}


/**
 * Add a prefix to the terms list and insert a separateor between them.
 *
 * @param $terms The pre-rendered HTML string containing the term list
 *        elements.
 * @param $prefix The text to show before the list of terms. By default the
 *        output of t('Tags: ') is used.
 * @param $separator The character(s) to place between the terms. By default,
 * 		  the output of t(', ') is used.
 *
 * @return The modified HTML.
 */
function camaxtli_separate_terms($terms, $prefix = NULL, $separator = NULL) {
  $prefix    = ($prefix == NULL) ? t('Tags: ') : $prefix;
  $separator = ($separator == NULL) ? t(', ') : $separator;
  $output    = $prefix . preg_replace('!a></li>\s<li!',
                                      "a>{$separator}</li>\n<li", $terms);
  return $output;
}


/**
 * Insert a separator between items in the list of links for a node.
 *
 * @param $links The pre-rendered HTML string containing the link list
 *        elements.
 * @param $separator character(s) to place between the links. By default, the
 *        output of t(' | ') is used.
 *
 * @return The modified HTML.
 */
function camaxtli_separate_links($links, $separator = ' | ') {
  $separator = ($separator == NULL) ? t(' | ') : $separator;
  $output    = preg_replace('!a></li>\s<li!',
                            "a>{$separator}</li>\n<li", $links);
  return $output;
}


/**
 * Preprocess the blocks.
 *
 * @param &$vars The template variables array. After invoking this function,
 *        these keys will be added to $vars:
 *        - 'camaxtli_block_id' - A complete unique ID for the block.
 *        - 'camaxtli_block_class' - The CSS classes to apply to the block. If
 *          the Block Class module is installed, those classes will be added
 *          to these.
 */
function camaxtli_preprocess_block(&$vars) {
  $block = &$vars['block'];

  $vars['camaxtli_block_id']    = 'block-' . $block->module .'-'. $block->delta;
  $vars['camaxtli_block_class'] = 'block block-' . $block->module .
                                ' block-' . $block->region;

  // --------------------------------------------------------------------------
  // -- Support the Block Class module if present.
  if (isset($vars['block_class'])) {
    $vars['camaxtli_block_class'] .= (' ' . $vars['block_class']);
  } elseif (function_exists('block_class')) {
    $vars['camaxtli_block_class'] .= (' ' . block_class($block));
  }
}


/**
 * Preprocess the nodes.
 *
 * @param &$vars The template variables array. After invoking this function,
 *        these keys will be added to $vars:
 *        - 'camaxtli_node_author' - The node's "posted by" text and author
 *          link.
 *        - 'camaxtli_node_class' - The CSS classes to apply to the node.
 *        - 'camaxtli_node_links' - The node links with a separator placed
 *          between each.
 *        - 'camaxtli_perma_title' - The localized permalink text for the node.
 *        - 'camaxtli_node_timestamp' - The timestamp for this type, if one
 *          should be rendered for this type.
 *        - 'camaxtli_term_links' - The taxonomy links with a separator placed
 *          between each.
 */
function camaxtli_preprocess_node(&$vars) {
  $node                         = $vars['node'];
  $vars['camaxtli_node_class']  = 'node ' . ($node->sticky ? 'sticky ' : '') .
                                ($node->status ? '' : ' node-unpublished') .
                                ' node-' . $node->type .
                                ($teaser ? ' teaser' : '') . ' clear-block';
  $vars['camaxtli_term_links']  = camaxtli_separate_terms($vars['terms']);
  $vars['camaxtli_node_links']  = camaxtli_separate_links($vars['links']);
  $vars['camaxtli_perma_title'] = t('Permanent Link to !title',
                                array('!title' => $vars['title']));

  // --------------------------------------------------------------------------
  // -- Node authorship.
  if (!empty($vars['submitted'])) {
    $vars['camaxtli_node_author'] = t('By !author',
                                      array('!author' => $vars['name']));
  }

  // --------------------------------------------------------------------------
  // -- Timestamp
  if (!empty($vars['submitted']) && isset($node->created)) {
    $vars['camaxtli_node_timestamp'] = format_date($node->created, 'custom', t('d M Y'));
  }

  // --------------------------------------------------------------------------
  // -- Extract the comment links.
  $links = $node->links;
  $comment = NULL;
  if (is_array($links) && array_key_exists('comment_comments', $links)) {
    $comment = $links['comment_comments'];
  }
  elseif (is_array($links) && array_key_exists('comment_add', $links)) {
    $comment          = $links['comment_add'];
    $comment['title'] = t('Leave a comment');
  }

  if (!empty($comment)) {
    $vars['camaxtli_comment'] = l($comment['title'], $comment['href'], $comment);
  }

  // --------------------------------------------------------------------------
  // -- Extract the read more link.
  if (is_array($links) && array_key_exists('node_read_more', $links)) {
    $read_more          = $links['node_read_more'];
    $read_more['title'] = t('Continue reading !arrow', array('!arrow' => '&rarr;'));
    $read_more['html']  = TRUE;
    $vars['camaxtli_readmore'] = l($read_more['title'], $read_more['href'], $read_more);
  }

  // --------------------------------------------------------------------------
  // -- Purge the link list.
  unset($vars['node']->links['comment_add']);
  unset($vars['node']->links['comment_comments']);
  unset($vars['node']->links['comment_new_comments']);
  unset($vars['node']->links['node_read_more']);

  // --------------------------------------------------------------------------
  // -- Re-render the $links variable
  $vars['links'] = theme_links($vars['node']->links);
}


/**
 * Provides page variables for the maintenance page.
 *
 * @param $vars The template variables array. After invoking this function,
 *        this array will have the same added values provided by
 *        camaxtli_preprocess_page.
 */
function camaxtli_preprocess_maintenance_page(&$vars) {
  $vars['in_maintenance'] = TRUE;
  camaxtli_preprocess_page($vars);
}


/**
 * Override or insert PHPTemplate variables into the templates.
 *
 * @param $vars The template variables array. After invoking this function,
 *        these keys will be added to $vars:
 *        - 'camaxtli_classes' - The CSS classes to apply to the content and
 *          sidebar regions. This array will have 'content', 'left', and 'right'
 *          as keys. The values will include the grid size for the region and
 *          any push/pull classes it needs.
 *        - 'camaxtli_logo' - The HTML for the linked logo image.
 *        - 'camaxtli_page_title' - The pre rendered page title element with the
 *          appropriate CSS classes assigned.
 *        - 'camaxtli_placement' - The theme setting for how the sidebars should
 *          be rendered relative to the content region. Will be one of: 'left',
 *          'center', or 'right'.
 *        - 'camaxtli_primary_links' - The HTML for the rendered primary links.
 *        - 'camaxtli_render_date' - whether or not to render the date for this
 *          type if the page is a node page.
 *        - 'camaxtli_secondary_links' - The HTML for the rendered secondary
 *          links.
 *        - 'camaxtli_slogan' - The HTML for the site slogan.
 *        - 'camaxtli_title' - The HTML for the linked title.
 *        - 'tabs2' - The HTML for the menu secondary local tasks.
 */
function camaxtli_preprocess_page(&$vars) {
  $vars['tabs2'] = menu_secondary_local_tasks();

  // --------------------------------------------------------------------------
  // -- Re-order the CSS files so that the framework styles come first.
  $vars['css_alt'] = camaxtli_css_reorder($vars['css']);
  $vars['styles'] = drupal_get_css($vars['css_alt']);


  // --------------------------------------------------------------------------
  // -- Optimize the site name to be better for search engines. If viewing as a
  // -- page, the site name is demoted to an H2 element instead of the default
  // -- H1 element. This CSS will render them identically.
  $vars['camaxtli_title'] = check_plain($vars['site_name']);
  $base = '<%s id="site-title"><a href="%s" title="%s">%s</a></%s>';
  $elem = empty($vars['title']) ? 'h1' : 'span';

  $vars['camaxtli_title'] = sprintf($base, $elem,
                                    check_url($vars['front_page']),
                                    $vars['site_name'],
                                    $vars['camaxtli_title'], $elem);

  // --------------------------------------------------------------------------
  // -- The primary links
  if (!empty($vars['primary_links'])) {
    $props = array(
      'id'    => 'primary-nav',
    );

    if (!empty($vars['camaxtli_secondary_links'])) {
      $props['class'] = 'grid-16 has-secondary';
    }

    $vars['camaxtli_primary_links'] = theme('links', $vars['primary_links'], $props);
  }

  // --------------------------------------------------------------------------
  // -- Pre-render the page title with the appropriate CSS classes.
  if ($vars['in_maintenance'] == TRUE) {
    $page_title = t('Site off-line for maintenance');
  }
  elseif ($vars['is_front'] == TRUE) {
    $page_title = t('Latest Posts');
    $elem = 'span';
  }
  elseif (!empty($vars['title'])) {
    $page_title = $vars['title'];
    $elem = 'h1';
  }
  else {
    $page_title = NULL;
  }

  if ($page_title != NULL) {
    $title_class = 'class="grid-12 alpha omega' .
                   ($vars['tabs'] ? ' with-tabs' : '') . '"';
    $title_pat   = '<%s id="page-title" %s>%s</%s>';
    $vars['camaxtli_page_title'] = sprintf($title_pat, $elem, $title_class,
                                           $page_title, $elem);
  }

  // --------------------------------------------------------------------------
  // -- The formatted date for this node, if the date should be rendered.
  $node = $vars['node'];
  if (!empty($node) && isset($node->created) && camaxtli_show_datestamp($node->type)) {
    $vars['camaxtli_node_timestamp'] = format_date($node->created, 'custom', t('d M Y'));
  }

}


/**
 * Overrides template_preprocess_comment().
 *
 * @param &$vars The template variables array. After invoking this function,
 *        these keys will be added to $vars:
 *        - 'camaxtli_author_date' - The formatted author link and date for the
 *          comment's meta data area.
 *        - 'camaxtli_comment_class' - The CSS classes to apply to this comment.
 *        - 'camaxtli_comment_links' - The comment links with a delimiter added.
 */
function camaxtli_preprocess_comment(&$vars) {
  $comment = $vars['comment'];
  $node    = $vars['node'];

  $vars['author']  = theme('username', $comment);
  $vars['content'] = $comment->comment;

  // --------------------------------------------------------------------------
  // -- Adjust the title link to have a title.
  $options = array(
    'fragment'   => "comment-$comment->cid",
    'attributes' => array(
      'title' => t('Permanent link to this comment.'),
    ),
  );

  $vars['title'] = l($comment->subject, $_GET['q'], $options);

  // --------------------------------------------------------------------------
  // -- The author and timestamp,
  $params = array(
    '@date'   => format_date($comment->timestamp, 'custom', t('j M Y')),
    '@time'   => format_date($comment->timestamp, 'custom', t('g:i a')),
  );

  $vars['camaxtli_comment_date'] = t('@date, @time', $params);


  // --------------------------------------------------------------------------
  // -- Add the separator characters between the links.
  $vars['camaxtli_comment_links'] = camaxtli_separate_links($vars['links']);

  // --------------------------------------------------------------------------
  // -- Determine the CSS classes for the comment.
  $author      = ($comment->uid == $node->uid) ? ' original-author' : '';
  $comment_new = ($comment->new) ? ' comment-new' : '';
  $logged_in   = isset($comment->uid) ? ' commenter-logged-in' : '';

  $vars['camaxtli_comment_class'] = "comment {$vars['status']} {$vars['zebra']}" .
                                  $comment_new . $logged_in . $author .
                                  ' clear-block';
}


/**
 * Returns the rendered local tasks. The default implementation renders
 * them as tabs. Overridden to split the secondary tasks.
 */
function phptemplate_menu_local_tasks() {
  return menu_primary_local_tasks();
}


/**
 * Generates IE CSS links for LTR and RTL languages.
 *
 * @return the IE style elements.
 */
function phptemplate_get_ie_styles() {
  global $language;

  $iecss = '<link type="text/css" rel="stylesheet" media="screen" href="' .
           base_path() . path_to_theme() .'/styles/fix-ie.css" />';
  if (defined('LANGUAGE_RTL') && $language->direction == LANGUAGE_RTL) {
    $iecss .= '<style type="text/css" media="screen">@import "' .
              base_path() . path_to_theme() .'/styles/fix-ie-rtl.css";</style>';
  }

  return $iecss;
}


/**
 * This rearranges how the style sheets are included so the framework styles
 * are included first.
 *
 * Sub-themes can override the framework styles when it contains css files with
 * the same name as a framework style. This can be removed once Drupal supports
 * weighted styles.
 *
 * @param $css the array of CSS files.
 *
 * @return the reordered array of CSS files.
 */
function camaxtli_css_reorder($css) {
  global $theme_info, $base_theme_info;

  // --------------------------------------------------------------------------
  // -- Dig into the framework .info data.
  $framework = !empty($base_theme_info) ? $base_theme_info[0]->info : $theme_info->info;

  // --------------------------------------------------------------------------
  // -- Pull framework styles from the themes .info file and place them above
  // -- all stylesheets.
  if (isset($framework['stylesheets'])) {
    foreach ($framework['stylesheets'] as $media => $styles_from_960) {
      // Setup framework group.
      if (isset($css[$media])) {
        $css[$media] = array_merge(array('framework' => array()), $css[$media]);
      }
      else {
        $css[$media]['framework'] = array();
      }
      foreach ($styles_from_960 as $style_from_960) {
        // Force framework styles to come first.
        if (strpos($style_from_960, 'framework') !== FALSE) {
          $framework_shift = $style_from_960;
          $remove_styles = array($style_from_960);
          // Handle styles that may be overridden from sub-themes.
          foreach ($css[$media]['theme'] as $style_from_var => $preprocess) {
            if ($style_from_960 != $style_from_var && basename($style_from_960) == basename($style_from_var)) {
              $framework_shift = $style_from_var;
              $remove_styles[] = $style_from_var;
              break;
            }
          }
          $css[$media]['framework'][$framework_shift] = TRUE;
          foreach ($remove_styles as $remove_style) {
            unset($css[$media]['theme'][$remove_style]);
          }
        }
      }
    }
  }

  return $css;
}
